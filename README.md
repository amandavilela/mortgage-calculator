# Mortgage Calculator

This is a project for the ConsumerAffairs coding challenge.
I hope we can work together!

## Technical details :sparkles:

- Mobile first;
- ES6+;
- Sass;
- BEM;

## Dependencies :construction_worker:

- NodeJS (install it with [nvm](https://github.com/creationix/nvm))
- Node Sass

## Running :rocket:

```$ git clone git@bitbucket.org:amandavilela/mortgage-calculator.git```

```$ cd mortgage-calculator```

```$ npm install```

```$ npm start```
