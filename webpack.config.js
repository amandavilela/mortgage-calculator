module.exports = {
    entry: {
        main: './js/calculator.js'
    },
    output: {
      path: __dirname,
      filename: "calculator.js"
    },
    devtool: 'source-map',
    module: {
        rules: [
          {
            test: /\.js$/,
            use: {
              loader: "babel-loader",
              options: { presets: ["es2015"] }
            }
          },
          {
            test: /\.scss$/,
            use: [
              {
                loader: "style-loader" // creates style nodes from JS strings
              },
              {
                loader: "css-loader" // translates CSS into CommonJS
              },
              {
                loader: "sass-loader" // compiles Sass to CSS
              }
            ]
          }
        ]
    }
}
