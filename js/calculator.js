import '../scss/style.scss';

(function() {

  if ('NodeList' in window && !NodeList.prototype.forEach) {
      console.info('polyfill for IE11');
      NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;
        for (var i = 0; i < this.length; i++) {
          callback.call(thisArg, this[i], i, this);
        }
      };
  }

  const yearsOfMortgage = document.querySelector('#years-of-mortgage');
  const rateOfInterest = document.querySelector('#rate-of-interest');
  const yearsOfMortgageValue = document.querySelector('#years-of-mortgage-value');
  const rateOfInterestValue = document.querySelector('#rate-of-interest-value');
  const calculator = document.querySelector('#calculator');

  calculator.addEventListener('submit', validateCalculatorEntries);

  yearsOfMortgage.addEventListener('change', () => {
    yearsOfMortgageValue.value = yearsOfMortgage.valueAsNumber;
    updateRange(yearsOfMortgage);
  });

  rateOfInterest.addEventListener('change', () => {
    rateOfInterestValue.value = rateOfInterest.valueAsNumber;
    updateRange(rateOfInterest);
  });

  yearsOfMortgageValue.addEventListener('change', () => {
    yearsOfMortgage.value = yearsOfMortgageValue.value;
    updateRange(yearsOfMortgage);
  });

  rateOfInterestValue.addEventListener('change', () => {
    rateOfInterest.value = rateOfInterestValue.value;
    updateRange(rateOfInterest);
  });

  function calculateMortgage() {
    const loanAmountValue = document.querySelector('#loan-amount').value;
    const annualInsuranceValue = document.querySelector('#annual-insurance').value;
    const annualTax = document.querySelector('#annual-tax').value;
    const results = document.querySelector('#results');
    const resultPrincipleAndInterest = document.querySelector('#principle-and-interest');
    const resultTax = document.querySelector('#tax');
    const resultInsurance = document.querySelector('#insurance');
    const resultTotal = document.querySelector('#total');

    const principleAndInterests = parseFloat((((rateOfInterestValue.value / 100) / 12) * loanAmountValue / (1-Math.pow((1 + ((rateOfInterestValue.value / 100)/12)),
  -yearsOfMortgageValue.value*12))).toFixed(2));

    const tax = parseFloat((annualTax / 12).toFixed(2));

    const insurance = parseFloat((annualInsuranceValue / 12).toFixed(2));

    const monthlyPayment = parseFloat((principleAndInterests + tax + insurance).toFixed(2));

    resultPrincipleAndInterest.innerHTML = `$ ${principleAndInterests}`;
    resultPrincipleAndInterest.classList.add('results__value--result');
    resultTax.innerHTML = `$ ${tax}`;
    resultTax.classList.add('results__value--result');
    resultInsurance.innerHTML = `$ ${insurance}`;
    resultInsurance.classList.add('results__value--result');
    resultTotal.innerHTML = `$ ${monthlyPayment}`;
    resultTotal.classList.add('results__value--result');

    results.classList.add('results--open');

    if (window.innerWidth < 1024) {
      window.scrollTo(document.body, 800);
    }
  }

  function addMessage(input) {
    const errorMessage = input.parentNode.querySelector('.calculator__error-message');
    const message = input.getAttribute('[data-error]');
    const error = document.createElement('span');

    input.classList.add('calculator__text-input--error');
    if (!message || errorMessage) return;

    error.textContent = message;
    error.className = 'calculator__error-message';
    input.parentNode.appendChild(error);
  }

  function removeMessage(input) {
    const errorMessage = input.parentNode.querySelector('.calculator__error-message');
    if (errorMessage) {
      errorMessage.remove();
      input.classList.remove('calculator__text-input--error');
    }
  }

  function validateCalculatorEntries(e) {
    const inputs = calculator.querySelectorAll('input');
    let isFormValid = true;

    e.preventDefault();

    inputs.forEach(input => {
      if (input.value.trim() === '') {
        isFormValid = false;
        addMessage(input);
      } else {
        removeMessage(input);
      }
    });

    if (isFormValid) {
      calculateMortgage();
    }
  }

  function updateRange(input) {
    const inputMin = input.getAttribute('min');
    const inputMax = input.getAttribute('max');
    const percentage = (inputMax - inputMin) / 100;
    input.style.setProperty('--value', (input.value - inputMin)/percentage);
  }

})();
